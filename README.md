# Multi-button Mouse Customization

## Instructions

Use xev to find out which mouse button is what.  (from Mouse Customizations.pdf)

install xdotools to work with compiz in binding buttons (from Mouse Customizations 2.pdf)
base action script is from Mouse Customizations 2.pdf
compiz instructions is from Mouse Customizations 2.pdf

Instructions for bash programing is in Mouse Customizations 3.pdf
